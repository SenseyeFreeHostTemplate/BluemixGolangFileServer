# Bluemix (IBM) Golang File Server template

##### Local run
```bash
go run server.go
```

##### Bluemix
```bash
bluemix app push golang-static-file-server
```

##### Example:
[https://golang-static-file-server.eu-gb.mybluemix.net/](https://golang-static-file-server.eu-gb.mybluemix.net/)

##### Documentation:
- [Write your first Golang app server on Bluemix](https://www.ibm.com/blogs/bluemix/2015/10/getting-started-with-golang-on-bluemix/)
- [GitHub IBM Hello World example](https://github.com/IBM-Cloud/go-hello-world)

##### Load testing
```bash
ab -k -c 100 -n 500 https://golang-static-file-server.eu-gb.mybluemix.net/
```
- [Stack Overflow ab load testing](https://stackoverflow.com/questions/12732182/ab-load-testing)
- [ab - Apache HTTP server benchmarking tool](http://httpd.apache.org/docs/2.2/programs/ab.html)

if you start this testing with more *concurrency* `-c` you [DDOS](https://www.cloudflare.com/learning/ddos/what-is-a-ddos-attack/) server and stop it for some time,
so http status code will be [404](https://en.wikipedia.org/wiki/HTTP_404):
```html
404 Not Found: Requested route ('golang-static-file-server.eu-gb.mybluemix.net') does not exist.
```
or [502](https://en.wikipedia.org/wiki/HTTP_502):
```html
502 Bad Gateway: Registered endpoint failed to handle the request.
```
it's fine, because only 16M RAM in [manifest.json](https://gitlab.com/SenseyeFreeHostTemplate/BluemixGolangFileServer/blob/master/manifest.yml)
