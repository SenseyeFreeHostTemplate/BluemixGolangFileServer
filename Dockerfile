FROM golang:1.10.3

ADD . /go/src/gitlab.com/SenseyeFreeHostTemplate/BluemixGolangFileServer

# Build the outyet command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN cd /gitlab.com/SenseyeFreeHostTemplate/BluemixGolangFileServer;make

# Run the outyet command by default when the container starts.
ENTRYPOINT cd /gitlab.com/SenseyeFreeHostTemplate/BluemixGolangFileServer/;./server

# Document that the service listens on port 8080.
EXPOSE 8080